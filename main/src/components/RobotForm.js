import React, { Component } from "react";

class RobotForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      type: "",
      mass: 0
    };

    this.handleChange = e => {
      this.setState({
        [e.target.name]: e.target.value
      });
    };
  }

  render() {
    return (
      <div>
        <input
          type="text"
          onChange={this.handleChange}
          placeholder="name"
          name="name"
          id="name"
        />
        <input
          type="text"
          onChange={this.handleChange}
          placeholder="type"
          name="type"
          id="type"
        />
        <input
          type="number"
          onChange={this.handleChange}
          placeholder="0"
          name="mass"
          id="mass"
        />
        <input
          type="button"
          value="add"
          onClick={() => {
            this.props.onAdd({
              name: this.state.name,
              type: this.state.type,
              mass: this.state.mass
            })}
          }
        />
      </div>
    );
  }
}

export default RobotForm;
